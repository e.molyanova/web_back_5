<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>web_back_5</title>
  <!--  <script defer src="https://code.jquery.com/jquery-1.11.0.min.js"></script>-->
 <!--   <script defer src="main.js"></script>  -->
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="облако.png" width="200" height="50"  type="image/x-icon">
  </head>
  <body>
  
      <li>
        <?php 
        if(!empty($_COOKIE[session_name()]) && !empty($_SESSION['login']))
          print('<a href="./?quit=1" title = "Выйти">Выйти</a>');
        else
          print('<a href="login.php" title = "Войти">Войти</a>');
        ?>
      </li>

  <div class="main">
      <?php 
    if (!empty($messages)) {
      print('<section id="messages">');
      if ($hasErrors)
        print('<h2>Ошибка</h2>');
      else{
                print('<h2>Без ошибок</h2>');
        }
      foreach ($messages as $message) {
        print($message);
      }
      print('</section>');
    }
    ?>
<div class="container">
<h2>Sign In Form</h2>
    <form id="form" name="myform" method="POST" action=".">
<p><div class="group"><label> Ваше имя:<INPUT type="text" placeholder="Имя" name="name" <?php if (!empty($errors['name'])) {print 'class="error"';} ?> value="<?php print $values['name']; ?>"/><span class="bar"></span> </label></p>
<p><label>Ваш email: <INPUT type="email" name="email" <?php if (!empty($errors['email'])) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" placeholder="E-mail" /></label></p>
<p><label>Ваша дата рождения: <INPUT type="date" name="birthday" <?php if (!empty($errors['birthday'])) {print 'class="error"';} ?> value="<?php print $values['birthday']; ?>" placeholder="Дата рождения" /></label></p>
<P>Ваш пол: <label><INPUT type="radio" name="gender" value="M" <?php if ($values['gender']=='M') {print 'checked';} ?> /> Мужской</label>
  <label><INPUT type="radio" name="gender" value="F" <?php if ($values['gender']=='F') {print 'checked';} ?> /> Женский</label></P>
<p>Количество ваших конечностей: <label><INPUT type="radio" <?php if ($values['limbs']=='1') {print 'checked' ;} ?> name="limbs" value="1" /> 3</label>
<label><INPUT type="radio" <?php if ($values['limbs']=='2') {print 'checked' ;} ?> name="limbs" value="2"/> 4</label>
<label><INPUT type="radio" <?php if ($values['limbs']=='3') {print 'checked' ;}?> name="limbs" value="3" /> 5</label>
</p>
<label>
<P>Ваши сверхспособности:
<SELECT name="abilitiess[]" multiple="multiple">
<OPTION value="1" <?php if ($values['abilitiess']['1']) {print 'selected' ;} ?>>Чтение мыслей</OPTION>
<OPTION value="2" <?php if ($values['abilitiess']['2']) {print 'selected' ;} ?>>Перерождение</OPTION>
<OPTION value="3" <?php if ($values['abilitiess']['3']) {print 'selected' ;} ?>> Управление солнечным светом</OPTION>
<OPTION value="4" <?php if ($values['abilitiess']['4']) {print 'selected' ;} ?>> Заговаривание воды</OPTION>
</SELECT>
</label></p>
<p><label>Биография: <textarea name="biography"><?php print $values['biography']; ?></textarea></label></p>
    <p> <label <?php if(array_key_exists('contract', $errors)) {print 'class="error"';}?>><input type="checkbox"
      name="contract" <?php if ($values['contract']) {print 'checked' ;} ?> />
      С контрактом ознакомлен(а)</label></p>

    
    <a class="button9"><input id="submit" type="submit" value="Отправить" /></a>
</form>
</div>
</div>
</body>
</html>

